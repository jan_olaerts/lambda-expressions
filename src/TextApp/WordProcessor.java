package TextApp;

@FunctionalInterface
public interface WordProcessor {
    public String process(String s);
}