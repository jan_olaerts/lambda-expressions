package TextApp;

@FunctionalInterface
public interface WordFilter {
    public boolean isValid(String s);
}