package TextApp;

public class TextScrambler {

    public String scramble(String s) {
        StringBuilder sb = new StringBuilder(s);
        for(int i = 0; i < sb.length(); i++) {
            if(sb.charAt(i) == 'a') sb.replace(i, i+1, "@");
            if(sb.charAt(i) == 'e') sb.replace(i, i+1, "€");
            if(sb.charAt(i) == 'i') sb.replace(i, i+1, "1");
            if(sb.charAt(i) == 'o') sb.replace(i, i+1, "0");
        }

        return sb.toString();
    }
}