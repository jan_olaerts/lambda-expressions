package TextApp;

import java.math.BigDecimal;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class TextApp {
    public static void main(String[] args) {
        TextPrinter textPrinter = new TextPrinter("Hello this is an example of one sentence containing words");
//        textPrinter.printFilteredWords((String s) -> {
//            int eCount = 0;
//            for(int i = 0; i < s.length(); i++) {
//                if(s.charAt(i) == 'e') eCount++;
//            }
//
//            return eCount == 2;
//        });

//        textPrinter.printProcessedWords((String s) -> TextUtil.reverse(s));
//        textPrinter.printProcessedWords(TextUtil::reverse);

//        TextScrambler scrambler = new TextScrambler();
//        textPrinter.printProcessedWords(s -> scrambler.scramble(s));

//        textPrinter.printProcessedWords(s -> s.toLowerCase());
//        textPrinter.printProcessedWords(String::toLowerCase);

//        TextPrinter textPrinter = new TextPrinter("145 236 9852 3658");
//        textPrinter.printNumberValues(s -> new BigDecimal(s));
//        textPrinter.printNumberValues(BigDecimal::new);

//        textPrinter.printSum(s -> new BigDecimal(s));

        // Werken met de Predicate<T> standard functional interface
        Predicate<String> condition1 = (String s) -> s.contains("e");
        Predicate<String> condition2 = (String s) -> s.contains("a");
        Predicate<String> condition3 = condition1.and(condition2);
        Consumer<String> printer = s -> System.out.println();
        textPrinter.printFilteredWords(condition3);

        // Werken met de Function<T, R> standard functional interface
        Function<String, String> procedure1 = (String s) -> s.toUpperCase();
        Function<String, String> procedure2 = (String s) -> TextUtil.reverse(s);
        Function<String, String> finalProcedure = procedure1.andThen(procedure2);
        textPrinter.printProcessedWords(finalProcedure);

        // Werken met de Consumer<T> standard functional interface
        textPrinter.printProcessedWords(s -> String.format("<<%s>>", s));
    }
}