package TextApp;

import java.math.BigDecimal;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class TextPrinter {

    private String sentence;
    private Consumer<String> consumer;

    public TextPrinter(String sentence) {
        this(sentence, System.out::println);
    }

    public TextPrinter(String sentence, Consumer<String> printer) {
        this.sentence = sentence;
        this.consumer = printer;
    }

    public void printFilteredWords(Predicate<String> filter) {
        for(String w : sentence.split(" ")) {
            if(filter.test(w)) filter.test(w);
        }
    }

    public void printProcessedWords(Function<String, String> processor) {
        for(String w : sentence.split(" ")) {
            this.consumer.accept(processor.apply(w));
        }
    }

    public void printNumberValues(Function<String, BigDecimal> parser) {
        for(String w : sentence.split(" ")) {
            System.out.println(parser.apply(w));
        }
    }

    public void printSum(Function<String, BigDecimal> parser) {
        int sum = 0;
        for(String w : sentence.split(" ")) {
            BigDecimal bd = parser.apply(w);
            sum += bd.intValue();
        }

        System.out.println(sum);
    }
}